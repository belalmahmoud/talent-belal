var gulp = require('gulp');
var uglify = require('gulp-uglify');
var livereload = require('gulp-livereload');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var imagemin = require('gulp-imagemin');
var imageminPngquant = require('imagemin-pngquant');
var imageminJpegRecompress = require('imagemin-jpeg-recompress');
var DIST_PATH = './dist';
var DIST_PATH_css = './dist/sass';
var DIST_PATH_img = './dist/img';
var DIST_PATH_js = './dist/js';
var html_path = 'src/*.html'
var SCRIPTS_PATH = 'src/js/**/*.js';
var IMAGES_PATH = 'src/img/**/*.{png,jpeg,jpg,svg,gif}';
gulp.task('styles', function () {
  console.log('starting styles task');
  return gulp.src('src/scss/*.scss')
    .pipe(plumber(function (err) {
      console.log('style Task Error');
      console.log(err);
      this.emit('end');
    }))
    .pipe(sourcemaps.init())
    .pipe(autoprefixer())
    .pipe(sass({
      outputStyle: 'compressed'
    }))
    .pipe(concat('src-main.scss'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(DIST_PATH_css))
    .pipe(livereload());
});
gulp.task('scripts', function () {
  console.log('starting scripts task');
  return gulp.src(['./node_modules/jquery/dist/jquery.js','./node_modules/jquery-migrate/dist/jquery-migrate.min.js','./node_modules/bootstrap/dist/js/bootstrap.js','./node_modules/slick-carousel/slick/slick.js',SCRIPTS_PATH])
    .pipe(plumber(function (err) {
      console.log('Scripts Task Error');
      console.log(err);
      this.emit('end');
    }))
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(concat('src-main.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(DIST_PATH_js))
    .pipe(livereload());
});
gulp.task('html',function () {
return gulp.src(html_path)
  .pipe(gulp.dest(DIST_PATH))
  .pipe(livereload());
})

gulp.task('images', function () {
  return gulp.src(IMAGES_PATH)
    .pipe(imagemin(
      [
        imagemin.gifsicle(),
        imagemin.jpegtran(),
        imagemin.optipng(),
        imagemin.svgo(),
        imageminPngquant(),
        imageminJpegRecompress()
      ]
    ))
    .pipe(gulp.dest(DIST_PATH_img))
    .pipe(livereload());
});




gulp.task('default', ['images', 'templates', 'styles', 'scripts'], function () {
  console.log('Starting default task');
});

gulp.task('watch', function () {
  console.log('Starting watch task');
  require('./server.js');
  livereload.listen();
  gulp.watch(SCRIPTS_PATH, ['scripts']);
  gulp.watch('src/scss/**/*.scss', ['styles']);
  gulp.watch(html_path, ['html']);
  gulp.watch(IMAGES_PATH, ['images']);
});
